<?php
/**
 * Reference Controller for 45 North Solutions REST Solution
 * 
 * @version v0.2.0
 * @author andy@45n.co
 */
namespace Custom\Controllers
{
    //Load 45 North Solutions REST Base Controller
    require(APPPATH .'libraries/restserver/v0.2.2/REST_Controller.php');

    class v1 extends \REST_Controller
    {
        /**
         * Settings for each HTTP Method
         * @var array 
         */
        protected $methods = array(
            'user_get' => array(
                'key' => false,
                'level' => 1,
                'log' => true,
                'limit' => 100
            ),
            'user_post' => array(
                'key' => false,
                'level' => 2,
                'log' => true,
                'limit' => 100
            ),
            'user_put' => array(
                'key' => true,
                'level' => 2,
                'log' => true,
                'limit' => 100
            ),
            'user_delete' => array(
                'key' => true,
                'level' => 2,
                'log' => true,
                'limit' => 100
            )
        );

        /**
         * Default response type
         * @var string
         */
        public $rest_format = 'json';

        //This is the constructor for the custom controller. Do not modify anything within
        //this function.
        function __construct()
        {
            parent::__construct();
        }

        /**
         * Dummy Method to make CP Happy. Never Used
         */
        function user()
        {
            //dummy method
            die('in user. this should never be called, else remap is not working');

        }

        /**
         * /cc/rest/v1/user/1
         */
        function user_get($id)
        {
            $data = array('method' => 'GET', 'returned: ' => $this->get('user'), 'id' => $id);
            $this->response($data);
        }

        /**
         * /cc/rest/v1/user/{1}
         */
        function user_post()
        {       
            $data = array('method' => 'POST', 'returned: ' => $this->post('id'), "user" => $this->get('user'));
            $this->response($data);
        }

        /**
         * /cc/rest/v1/user/{1}
         */
        function user_put()
        {       
            $data = array('method' => 'PUT', 'returned: ' => $this->put('id'));
            $this->response($data);
        }

        /**
         * /cc/rest/v1/user/{1}
         */
        function user_delete()
        {
            $data = array('method' => 'DELETE', 'returned: ' => $this->delete('id'));
            $this->response($data);
        }


    }
}

/**
 * Shim Class for CP2 compatability
 */
namespace{
    class v1 extends \Custom\Controllers\v1
    {

    }
}