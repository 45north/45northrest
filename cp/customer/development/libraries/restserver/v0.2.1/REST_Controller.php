<?php
/**
 * 45 North Solutions REST Base Controller.
 * Based off of Phil Sturgeon's restserver project
 * https://github.com/philsturgeon/codeigniter-restserver
 * but shoehorned into CP3 and CP2
 * 
 * @version v0.2.1
 * @author andy@45n.co
 */

/**
 * Shim for CP2. Define the CP3 Base class as a ghost glass.
 */
namespace RightNow\Controllers 
{
    if(!class_exists('\RightNow\Controllers\Base'))
    {
        class Base extends \ControllerBase
        {
        }    
    }
}

/**
 * REST server base controller
 */
namespace {
    use RightNow\Utils\Framework;


    abstract class REST_Controller extends \RightNow\Controllers\Base
    {

        protected $rest_format = NULL; // Set this in a controller to use a default format
        protected $methods = array(); // contains a list of method properties such as limit, log and level
        protected $request = NULL; // Stores accept, language, body, headers, etc
        protected $rest = NULL; // Stores DB, keys, key level, etc
        private $_get_args = array();
        private $_post_args = array();
        private $_put_args = array();
        private $_delete_args = array();
        private $_args = array();
        private $_allow = TRUE;
        private $_allow_error_msg = "Invalid API Key";
        // List all supported methods, the first will be the default format
        private $_supported_formats = array(
            //'xml' 		=> 'application/xml',
            //'rawxml' 	=> 'application/xml',
            'json' => 'application/json',
            'jsonp' => 'application/json',
            'serialize' => 'application/vnd.php.serialized',
            'php' => 'text/plain',
            //'html' 		=> 'text/html',
            'csv' => 'application/csv'
        );

        // Constructor function
        function __construct()
        {
            parent::__construct();
            
            // Lets grab the config and get ready to party
            require(APPPATH . 'libraries/restserver/v0.2.1/rest.php');

            //Figure out which CPHP version is available
            if(!defined('CONNECT_NAMESPACE_PREFIX'))
            {
                /* Bootstrap Connect */
                require_once(get_cfg_var('doc_root') . '/ConnectPHP/Connect_init.php');
                initConnectAPI();

                if(class_exists('RightNow\Connect\v1_1\Contact'))
                {
                    define('CONNECT_NAMESPACE_PREFIX', 'RightNow\Connect\v1_1');
                }
                elseif(class_exists('RightNow\Connect\v1\Contact'))
                {
                    define('CONNECT_NAMESPACE_PREFIX', 'RightNow\Connect\v1');
                }
            }

            // How is this request being made? POST, DELETE, GET, PUT?
            $this->request->method = $this->_detect_method();

            // Which format should the data be returned in?
            $this->request->format = $this->_detect_format();

            // Which format should the data be returned in?
            $this->request->lang = $this->_detect_lang();

            // Checking for keys? GET TO WORK!
            if ($this->config->item('rest_enable_keys'))
            {
                $this->_allow = $this->_detect_api_key();
            }
        }

        /*
         * Remap
         *
         * Requests are not made to methods directly The request will be for an "object".
         * this simply maps the object and method to the correct Controller method.
         */

        public function _remap($object_called)
        {

            $controller_method = $object_called . '_' . $this->request->method;

            // Do we want to log this method (if allowed by config)?
            $log_method = !(isset($this->methods[$controller_method]['log']) AND $this->methods[$controller_method]['log'] == FALSE);

            // Use keys for this method?
            $use_key = !(isset($this->methods[$controller_method]['key']) AND $this->methods[$controller_method]['key'] == FALSE);

            // Get that useless shitty key out of here
            if ($this->config->item('rest_enable_keys') AND $use_key AND $this->_allow === FALSE)
            {
                if ($this->config->item('rest_enable_logging') AND $log_method)
                {
                    $this->_log_request($this->_allow);
                }
                $this->response(array('status' => 0, 'error' => $this->_allow_error_msg), 403);
                return;
            }


            // Sure it exists, but can they do anything with it?
            if (!method_exists($this, $controller_method))
            {
                $this->response(array('status' => 0, 'error' => 'Unknown method.'), 404);
                return;
            }

            //Make sure it's public
            $reflection = new ReflectionMethod($this, $controller_method);
            if (!$reflection->isPublic()) {
                $this->response(array('status' => 0, 'error' => 'Unknown method.'), 404);
                return;
            }


            // Doing key related stuff? Can only do it if they have a key right?
            if ($this->config->item('rest_enable_keys') AND ! empty($this->rest->key))
            {
                // Check the limit
                if ($this->config->item('rest_enable_limits') AND ! $this->_check_limit($controller_method))
                {
                    $this->response(array('status' => 0, 'error' => 'This API key has reached the hourly limit for this method.'), 401);
                    return;
                }

                // If no level is set use 0, they probably aren't using permissions
                $level = isset($this->methods[$controller_method]['level']) ? $this->methods[$controller_method]['level'] : 0;

                // If no level is set, or it is lower than/equal to the key's level
                $authorized = $level <= $this->rest->level;

                // IM TELLIN!
                if ($this->config->item('rest_enable_logging') AND $log_method)
                {
                    $this->_log_request($authorized);
                }

                // They don't have good enough perms
                if (!$authorized)
                {
                    $this->response(array('status' => 0, 'error' => 'This API key does not have enough permissions.'), 401);
                    return;
                }
            }

            // No key stuff, but record that stuff is happening
            else if ($this->config->item('rest_enable_logging') AND $log_method)
            {
                $this->_log_request($authorized = TRUE);
            }

            //
            // Now that all security Checks have passed, crunch the request ========
            //

            // Some Methods cant have a body
            $this->request->body = NULL;

            switch ($this->request->method)
            {
                case 'get':
                    // Grab proper GET variables
                    parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $get);

                    // If there are any, populate $this->_get_args
                    empty($get) OR $this->_get_args = $get;

                    break;

                case 'post':
                    $this->_post_args = $_POST;

                    // It might be a HTTP body
                    $this->request->body = file_get_contents('php://input');

                    if (!$_POST && $this->request->body)
                    {
                        $this->_post_args = json_decode($this->request->body);
                    }

                    break;

                case 'put':
                    // It might be a HTTP body
                    $this->request->body = file_get_contents('php://input');

                    // Try and set up our PUT variables anyway in case its not
                    if ($this->request->body && !($this->_put_args = json_decode($this->request->body)))
                    {
                        parse_str($this->request->body, $this->_put_args);
                    }
                    break;

                case 'delete':
                    // It might be a HTTP body
                    $this->request->body = file_get_contents('php://input');

                    // Try and set up our PUT variables anyway in case its not
                    if ($this->request->body && !($this->_delete_args = json_decode($this->request->body)))
                    {
                        parse_str($this->request->body, $this->_delete_args);
                    }
                    break;
            }
            
            //Get ID param
            $idParameterSegment = array_search($object_called, $this->uri->router->segments, true);
            $idValue = null;
            //If the session parameter was found, grab the value in the next segment
            if($idParameterSegment !== false)
                $idValue = $this->uri->router->segments[$idParameterSegment + 1];

            
            // Set up our GET variables
            $this->_get_args = @array_merge($this->_get_args, $this->uri->ruri_to_assoc($idParameterSegment));

            // Merge both for one mega-args variable
            $this->_args = @array_merge($this->_get_args, $this->_put_args, $this->_post_args, $this->_delete_args);

            
            // And...... GO!
            $this->$controller_method($idValue);
        }

        /*
         * response
         *
         * Takes pure data and optionally a status code, then creates the response
         */

        protected function response($data = array(), $http_code = 200)
        {
            if (empty($data))
            {
                $this->set_status_header(404);
                return;
            }

            $this->set_status_header($http_code);

            // If the format method exists, call and return the output in that format
            if (method_exists($this, '_format_' . $this->request->format))
            {
                // Set the correct format header
                $this->output->set_header('Content-type: ' . $this->_supported_formats[$this->request->format]);

                $formatted_data = $this->{'_format_' . $this->request->format}($data);
                $this->output->set_output($formatted_data);
            }

            // Format not supported, output directly
            else
            {
                $this->output->set_output($data);
            }
        }

        /*
         * Detect format
         *
         * Detect which format should be used to output the data
         */

        private function _detect_format()
        {
            $pattern = '/\.(' . implode('|', array_keys($this->_supported_formats)) . ')$/';

            // Check if a file extension is used
            if (preg_match($pattern, end($this->_get_args), $matches))
            {
                // The key of the last argument
                $last_key = end(array_keys($this->_get_args));

                // Remove the extension from arguments too
                $this->_get_args[$last_key] = preg_replace($pattern, '', $this->_get_args[$last_key]);
                $this->_args[$last_key] = preg_replace($pattern, '', $this->_args[$last_key]);

                return $matches[1];
            }

            // A format has been passed as an argument in the URL and it is supported
            if (isset($this->_args['format']) AND isset($this->_supported_formats))
            {
                return $this->_args['format'];
            }

            // Otherwise, check the HTTP_ACCEPT (if it exists and we are allowed)
            if ($this->config->item('rest_ignore_http_accept') === FALSE AND $this->input->server('HTTP_ACCEPT'))
            {
                // Check all formats against the HTTP_ACCEPT header
                foreach (array_keys($this->_supported_formats) as $format)
                {
                    // Has this format been requested?
                    if (strpos($this->input->server('HTTP_ACCEPT'), $format) !== FALSE)
                    {
                        // If not HTML or XML assume its right and send it on its way
                        if ($format != 'html' AND $format != 'xml')
                        {

                            return $format;
                        }

                        // HTML or XML have shown up as a match
                        else
                        {
                            // If it is truely HTML, it wont want any XML
                            if ($format == 'html' AND strpos($this->input->server('HTTP_ACCEPT'), 'xml') === FALSE)
                            {
                                return $format;
                            }

                            // If it is truely XML, it wont want any HTML
                            elseif ($format == 'xml' AND strpos($this->input->server('HTTP_ACCEPT'), 'html') === FALSE)
                            {
                                return $format;
                            }
                        }
                    }
                }
            } // End HTTP_ACCEPT checking
            // Well, none of that has worked! Let's see if the controller has a default
            if (!empty($this->rest_format))
            {
                return $this->rest_format;
            }

            // Just use the default format
            return $this->config->item('rest_default_format');
        }

        /*
         * Detect method
         *
         * Detect which method (POST, PUT, GET, DELETE) is being used
         */

        private function _detect_method()
        {
            $method = strtolower($this->input->server('REQUEST_METHOD'));

            if ($this->config->item('enable_emulate_request'))
            {
                if ($this->input->server('HTTP_X_HTTP_METHOD_OVERRIDE'))
                {
                    $method = strtolower($this->input->server('HTTP_X_HTTP_METHOD_OVERRIDE'));
                }
            }

            if (in_array($method, array('get', 'delete', 'post', 'put')))
            {
                return $method;
            }

            return 'get';
        }

        /*
         * Detect API Key
         *
         * See if the user has provided an API key
         */

        private function _detect_api_key()
        {
            // Work out the name of the SERVER entry based on config
            $key_name = 'HTTP_' . strtoupper(str_replace('-', '_', $this->config->item('rest_key_name')));

            $this->rest->key = NULL;
            $this->rest->level = NULL;
            $this->rest->ignore_limits = FALSE;

            // Find the key from server or arguments
            if ($key = ($this->input->get('API-Key')) ? : $this->input->server($key_name))
            {
                /* @var $contact RightNow\Connect\v1_2\Contact */
                try
                {
                    $token_class = CONNECT_NAMESPACE_PREFIX . '\REST\APIToken';
                    $token = $token_class::fetch(str_replace("'", "''", $key));

                    if (!$token)
                    {
                        $this->_allow_error_msg = "Invalid API Key";
                        return false;
                    } 
                    
                    if ($token->IsDisabled)
                    {
                        $this->_allow_error_msg = "API Key has been disabled";
                        return false;
                    } 
                    
                    if ($token->ValidIPs)
                    {
                        $ips = array_map('trim', explode(',', $token->ValidIPs));
                        if(array_search($this->input->ip_address(), $ips) === false)
                        {
                            $this->_allow_error_msg = "Invalid IP Address";
                            return false;
                        }
                    }
                    
                    //All is well with the world
                    $this->rest->token = $token;
                    $this->rest->key = $token->Token;
                    $this->rest->level = (int) $token->PermissionLevel;
                    $this->rest->ignore_limits = (bool) $token->IgnoreLimits;
                    return TRUE;
                    
                } catch (\Exception $ex)
                {
                    $this->_allow_error_msg = "Invalid API Key";
                    $this->_logMessage($ex);
                }
            }

            // No key has been sent
            return FALSE;
        }

        /*
         * Detect language(s)
         *
         * What language do they want it in?
         */

        private function _detect_lang()
        {
            if (!$lang = $this->input->server('HTTP_ACCEPT_LANGUAGE'))
            {
                return NULL;
            }

            // They might have sent a few, make it an array
            if (strpos($lang, ',') !== FALSE)
            {
                $langs = explode(',', $lang);

                $return_langs = array();
                $i = 1;
                foreach ($langs as $lang)
                {
                    // Remove weight and strip space
                    list($lang) = explode(';', $lang);
                    $return_langs[] = trim($lang);
                }

                return $return_langs;
            }

            // Nope, just return the string
            return $lang;
        }

        /*
         * Log request
         *
         * Record the entry for awesomeness purposes
         */

        private function _log_request($authorized = FALSE)
        {
            try
            {
                $request_class = CONNECT_NAMESPACE_PREFIX . '\REST\AccessLog';
                $request = new $request_class();
                $request->RequestURI = substr($this->uri->uri_string(), 0, 255);
                $request->Method = substr($this->request->method, 0, 16);
                if ($this->rest->token)
                {
                    $request->APIToken = $this->rest->token;
                    $request->ContactThumbprint = substr(sprintf("ID: %d, Login: %s, Token: %s", $this->rest->token->Contact->ID, $this->rest->token->Contact->Login, $this->rest->token->Token), 0, 255);
                }
                $request->IPAddress = $this->input->ip_address();
                $request->Authorized = $authorized;

                $request->save();
            } catch (\Exception $ex)
            {
                $this->_logMessage($ex);
            }
        }

        /*
         * Log request
         *
         * Record the entry for awesomeness purposes
         */

        private function _check_limit($controller_method)
        {
            // They are special, or it might not even have a limit
            if ($this->rest->ignore_limits OR ! isset($this->methods[$controller_method]['limit']))
            {
                // On your way sonny-jim.
                return TRUE;
            }

            try
            {

                // How many times can you get to this method an hour?
                $limit = $this->methods[$controller_method]['limit'];

                $now = (int) date('YzH');

                if ($now == $this->rest->token->LimitTimestamp)
                {
                    $count = (int) $this->rest->token->LimitCount;

                    // Your luck is out, you've called too many times!
                    if ($count > $limit)
                    {
                        return FALSE;
                    }

                    $this->rest->token->LimitCount = $count + 1;
                    $this->rest->token->save();
                } else
                {
                    $this->rest->token->LimitTimestamp = $now;
                    $this->rest->token->LimitCount = 1;
                    $this->rest->token->save();
                }

                return TRUE;
            } catch (\Exception $ex)
            {
                $this->_logMessage($ex);
                return FALSE; //Play it safe. return false
            }
        }

        // INPUT FUNCTION --------------------------------------------------------------

        protected function get($key = NULL, $xss_clean = TRUE)
        {
            if ($key === NULL)
            {
                return $this->_get_args;
            }

            return array_key_exists($key, $this->_get_args) ? $this->_xss_clean($this->_get_args[$key], $xss_clean) : FALSE;
        }

        protected function post($key = NULL, $xss_clean = TRUE)
        {
            if ($key === NULL)
            {
                return $this->_post_args;
            }
            if (is_array($this->_post_args))
            {
                return array_key_exists($key, $this->_post_args) ? $this->_xss_clean($this->_post_args[$key], $xss_clean) : FALSE;
            } elseif (is_object($this->_post_args))
            {
                return property_exists($this->_post_args, $key) ? $this->_xss_clean($this->_post_args->$key, $xss_clean) : FALSE;
            }
        }

        protected function put($key = NULL, $xss_clean = TRUE)
        {
            if ($key === NULL)
            {
                return $this->_put_args;
            }

            if (is_array($this->_put_args))
            {
                return array_key_exists($key, $this->_put_args) ? $this->_xss_clean($this->_put_args[$key], $xss_clean) : FALSE;
            } elseif (is_object($this->_put_args))
            {
                return property_exists($this->_put_args, $key) ? $this->_xss_clean($this->_put_args->$key, $xss_clean) : FALSE;
            }
        }

        protected function delete($key = NULL, $xss_clean = TRUE)
        {
            if ($key === NULL)
            {
                return $this->_delete_args;
            }

            if (is_array($this->_delete_args))
            {
                return array_key_exists($key, $this->_delete_args) ? $this->_xss_clean($this->_delete_args[$key], $xss_clean) : FALSE;
            } elseif (is_object($this->_delete_args))
            {
                return property_exists($this->_delete_args, $key) ? $this->_xss_clean($this->_delete_args->$key, $xss_clean) : FALSE;
            }
        }

        protected function _xss_clean($val, $bool)
        {
            if (CI_VERSION < 2)
            {
                return $bool ? $this->input->xss_clean($val) : $val;
            } else
            {
                return $bool ? $this->security->xss_clean($val) : $val;
            }
        }

        protected function validation_errors()
        {
            $string = strip_tags($this->form_validation->error_string());

            return explode("\n", trim($string, "\n"));
        }

        // SECURITY FUNCTIONS ---------------------------------------------------------
        // Force it into an array
        private function _force_loopable($data)
        {
            // Force it to be something useful
            if (!is_array($data) AND ! is_object($data))
            {
                $data = (array) $data;
            }

            return $data;
        }

        // FORMATING FUNCTIONS ---------------------------------------------------------
        // Format XML for output
        private function X_format_xml($data = array(), $structure = NULL, $basenode = 'xml')
        {

            // turn off compatibility mode as simple xml throws a wobbly if you don't.
            if (ini_get('zend.ze1_compatibility_mode') == 1)
            {
                ini_set('zend.ze1_compatibility_mode', 0);
            }

            if ($structure == NULL)
            {
                $structure = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$basenode />");
            }

            // loop through the data passed in.
            $data = $this->_force_loopable($data);
            foreach ($data as $key => $value)
            {
                // no numeric keys in our xml please!
                if (is_numeric($key))
                {
                    // make string key...
                    //$key = "item_". (string) $key;
                    $key = "item";
                }

                // replace anything not alpha numeric
                $key = preg_replace('/[^a-z_]/i', '', $key);

                // if there is another array found recrusively call this function
                if (is_array($value) OR is_object($value))
                {
                    $node = $structure->addChild($key);
                    // recrusive call.
                    $this->_format_xml($value, $node, $basenode);
                } else
                {
                    // Actual boolean values need to be converted to numbers
                    is_bool($value) AND $value = (int) $value;

                    // add single node.
                    $value = htmlentities($value, ENT_NOQUOTES, "UTF-8");

                    $UsedKeys[] = $key;

                    $structure->addChild($key, $value);
                }
            }

            // pass back as string. or simple xml object if you want!
            return $structure->asXML();
        }

        // Format Raw XML for output
        private function X_format_rawxml($data = array(), $structure = NULL, $basenode = 'xml')
        {
            // turn off compatibility mode as simple xml throws a wobbly if you don't.
            if (ini_get('zend.ze1_compatibility_mode') == 1)
            {
                ini_set('zend.ze1_compatibility_mode', 0);
            }

            if ($structure == NULL)
            {
                $structure = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$basenode />");
            }

            // loop through the data passed in.
            $data = $this->_force_loopable($data);
            foreach ($data as $key => $value)
            {
                // no numeric keys in our xml please!
                if (is_numeric($key))
                {
                    // make string key...
                    //$key = "item_". (string) $key;
                    $key = "item";
                }

                // replace anything not alpha numeric
                $key = preg_replace('/[^a-z0-9_-]/i', '', $key);

                // if there is another array found recrusively call this function
                if (is_array($value) OR is_object($value))
                {
                    $node = $structure->addChild($key);
                    // recrusive call.
                    $this->_format_rawxml($value, $node, $basenode);
                } else
                {
                    // Actual boolean values need to be converted to numbers
                    is_bool($value) AND $value = (int) $value;

                    // add single node.
                    $value = htmlentities($value, ENT_NOQUOTES, "UTF-8");

                    $UsedKeys[] = $key;

                    $structure->addChild($key, $value);
                }
            }

            // pass back as string. or simple xml object if you want!
            return $structure->asXML();
        }

        // Format HTML for output
        private function X_format_html($data = array())
        {
            // Multi-dimentional array
            if (isset($data[0]))
            {
                $headings = array_keys($data[0]);
            }

            // Single array
            else
            {
                $headings = array_keys($data);
                $data = array($data);
            }

            $this->load->library('table');

            $this->table->set_heading($headings);

            foreach ($data as &$row)
            {
                $this->table->add_row($row);
            }

            return $this->table->generate();
        }

        // Format HTML for output
        private function _format_csv($data = array())
        {
            // Multi-dimentional array
            if (isset($data[0]))
            {
                $headings = array_keys($data[0]);
            }

            // Single array
            else
            {
                $headings = array_keys($data);
                $data = array($data);
            }

            $output = implode(',', $headings) . "\r\n";
            foreach ($data as &$row)
            {
                $output .= '"' . implode('","', $row) . "\"\r\n";
            }

            return $output;
        }

        // Encode as JSON
        private function _format_json($data = array())
        {
            return json_encode($data);
        }

        // Encode as JSONP
        private function _format_jsonp($data = array())
        {
            return $this->get('callback') . '(' . json_encode($data) . ')';
        }

        // Encode as Serialized array
        private function _format_serialize($data = array())
        {
            return serialize($data);
        }

        // Encode raw PHP
        private function _format_php($data = array())
        {
            return var_export($data, TRUE);
        }

        /**
         * Set HTTP Status Header
         *
         * @access	public
         * @param	int		the status code
         * @param	string
         * @return	void
         */
        protected function set_status_header($code = 200, $text = '')
        {
            $stati = array(
                200 => 'OK',
                201 => 'Created',
                202 => 'Accepted',
                203 => 'Non-Authoritative Information',
                204 => 'No Content',
                205 => 'Reset Content',
                206 => 'Partial Content',
                300 => 'Multiple Choices',
                301 => 'Moved Permanently',
                302 => 'Found',
                304 => 'Not Modified',
                305 => 'Use Proxy',
                307 => 'Temporary Redirect',
                400 => 'Bad Request',
                401 => 'Unauthorized',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported'
            );

            if ($code == '' OR ! is_numeric($code))
            {
                show_error('Status codes must be numeric', 500);
            }

            if (isset($stati[$code]) AND $text == '')
            {
                $text = $stati[$code];
            }

            if ($text == '')
            {
                show_error('No status text available.  Please check your status code number or supply your own message text.', 500);
            }

            $server_protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : FALSE;

            if (substr(php_sapi_name(), 0, 3) == 'cgi')
            {
                header("Status: {$code} {$text}", TRUE);
            } elseif ($server_protocol == 'HTTP/1.1' OR $server_protocol == 'HTTP/1.0')
            {
                header($server_protocol . " {$code} {$text}", TRUE, $code);
            } else
            {
                header("HTTP/1.1 {$code} {$text}", TRUE, $code);
            }
        }

        /**
         * Framework agnostic log message
         * @param string $message
         */
        protected function _logMessage($message)
        {
            //Are you CP3
            if(method_exists('Framework', 'logMessage'))
            {
                Framework::logMessage($message);
            }
            //You must be CP2 then
            else
            {
                \logMessage($message);
            }

        }

    }
}    